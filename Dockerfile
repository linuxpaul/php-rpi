FROM resin/rpi-raspbian:jessie
MAINTAINER linuxpaul <linuxpaul@hotmail.de> 

ENV DEBIAN_FRONTEND noninteractive
ADD dotdeb.gpg .
RUN apt-key add dotdeb.gpg
RUN echo 'deb http://repozytorium.mati75.eu/raspbian jessie-backports main' > /etc/apt/sources.list.d/dotdeb.list

RUN apt-get update

RUN apt-get install -y --no-install-recommends php7.0 php7.0-curl php7.0-gd php7.0-fpm php7.0-cli php7.0-opcache \
php7.0-mbstring php7.0-xml php7.0-zip php7.0-apcu php7.0-mysql mysql-client; apt-get clean

COPY www.conf /etc/php/7.0/fpm/pool.d/
COPY php.ini /etc/php/7.0/fpm/

ENTRYPOINT ["php-fpm7.0", "-F"]

